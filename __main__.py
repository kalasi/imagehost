#!/usr/bin/env python3

# Load environment variables before anything else.
from __future__ import unicode_literals
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

from flask import Flask, request, send_from_directory
from os import environ
import os
from random import SystemRandom
import random
import string
import time
from werkzeug.utils import secure_filename

# Prepare uploads folder.
if not os.path.exists(environ.get('UPLOAD_DIR')):
    os.makedirs(environ.get('UPLOAD_DIR'))

ALLOWED_EXTENSIONS = environ.get('EXTENSIONS').split(',')

app = Flask(__name__)

def allowed_file(filename):
    """ Check that the given filename's extension is valid.

    Args:
        filename: the filename whose extension to check.

    Returns:
        A bool of whether the extension of the filename is in the
        environment-defined list of valid extensions.
    """

    try:
        return filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS
    except IndexError:
        return False

@app.route('/', methods=['GET'])
def index():
    """ Outputs the index page."""

    return """
<!DOCTYPE html>
<html>
  <head>
    <title>Temporary file hosting</title>
  </head>
  <body>
    <h1>Upload new file</h1>
    <form action='' method='post' enctype='multipart/form-data'>
      <input type='file' name='file'>
      <input type='submit'>
    </form>
  </body>
</html>"""


@app.route('/', methods=['POST'])
def post():
    """Saves an uploaded file if it meets all conditions.

    An image is uploaded and saved, prefixed with a random 3-character prefix
    along with an underscore (`_`) prior.

    The conditions for a file to be successfully met are:

    - a file is sent;
    - the file has a filename;
    - the file is of an appropriate extension.
    """

    # Check that a file was sent.
    try:
        f = request.files['file']
    except KeyError:
        return 'no file sent'

    # Check that the file has a filename.
    if f.filename == '':
        return 'no filename'

    # Check that the file's extension is appropriate.
    if not f or not allowed_file(f.filename):
        return 'file type not allowed: {}'.format(', '.join(ALLOWED_EXTENSIONS))

    # Generate a random 3-char hash prefixed to the filename.
    choices = string.ascii_lowercase + string.ascii_uppercase + string.digits
    h = ''.join(SystemRandom().choice(choices) for _ in range(3))
    filename = '{}_{}'.format(h, secure_filename(f.filename))

    # Save the file in the upload directory.
    f.save(os.path.join(environ.get('UPLOAD_DIR'), filename))

    tls = 's' if environ.get("TLS") == 'yes' else ''
    domain = environ.get('DOMAIN')
    url = 'http{}://{}/{}'.format(tls, domain, filename)

    return "<a href='/{}'>link</a>\n{}".format(filename, url)

@app.route('/<filename>')
def uploaded_file(filename):
    """ Send an uploaded file if requested.

    This should be overrode by the HTTP server's config."""

    return send_from_directory(environ.get('UPLOAD_DIR'), filename)

app.run(host=environ.get('HOST'), port=int(environ.get('PORT')))
